<div class="footer py-4 d-flex flex-column flex-md-row align-items-center justify-content-between" id="kt_footer">
						<!--begin::Copyright-->
						<div class="order-2 order-md-1">
							<span class="text-white opacity-75 fw-bold me-1">2022©</span>
							<a href="https://webclixs.co.in/" target="_blank" class="text-white text-hover-primary opacity-75">Webxclix</a>
						</div>
						<!--end::Copyright-->
						<!--begin::Menu-->
						<ul class="menu menu-white menu-hover-primary fw-bold order-1 opacity-75">
							<li class="menu-item">
								<a href="https://webclixs.co.in/" target="_blank" class="menu-link px-2">About</a>
							</li>
							<!-- <li class="menu-item">
								<a href="https://devs.keenthemes.com/" target="_blank" class="menu-link px-2">Support</a>
							</li>
							<li class="menu-item">
								<a href="https://1.envato.market/EA4JP" target="_blank" class="menu-link px-2">Purchase</a>
							</li> -->
						</ul>
						<!--end::Menu-->
					</div>


